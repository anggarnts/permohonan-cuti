PGDMP                          x            PermohonanCuti    12.2    12.2 "    5           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            6           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            7           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            8           1262    20166    PermohonanCuti    DATABASE     �   CREATE DATABASE "PermohonanCuti" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
     DROP DATABASE "PermohonanCuti";
                postgres    false            �            1259    20167    bucket_approval    TABLE     `  CREATE TABLE public.bucket_approval (
    bucket_approval_id bigint NOT NULL,
    nama_pemohon character varying(255),
    created_at timestamp without time zone,
    created_by character varying(255),
    tanggal_pengajuan date,
    resolved_by character varying(255) NOT NULL,
    resolved_date date NOT NULL,
    pengambil_keputusan character varying(255) NOT NULL,
    alasan_keputusan character varying(255) NOT NULL,
    submission_status character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    user_id bigint,
    user_leave_request_id bigint
);
 #   DROP TABLE public.bucket_approval;
       public         heap    postgres    false            �            1259    20207 &   bucket_approval_bucket_approval_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bucket_approval_bucket_approval_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.bucket_approval_bucket_approval_id_seq;
       public          postgres    false            �            1259    20175    position    TABLE       CREATE TABLE public."position" (
    position_id bigint NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    position_name character varying(255) NOT NULL,
    updated_at timestamp without time zone,
    updated_by character varying(255)
);
    DROP TABLE public."position";
       public         heap    postgres    false            �            1259    20183    position_leave    TABLE     6  CREATE TABLE public.position_leave (
    position_leave_id bigint NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    jatah_cuti integer NOT NULL,
    updated_at timestamp without time zone,
    updated_by character varying(255),
    position_id bigint NOT NULL
);
 "   DROP TABLE public.position_leave;
       public         heap    postgres    false            �            1259    20209 $   position_leave_position_leave_id_seq    SEQUENCE     �   CREATE SEQUENCE public.position_leave_position_leave_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.position_leave_position_leave_id_seq;
       public          postgres    false            �            1259    20211    position_position_id_seq    SEQUENCE     �   CREATE SEQUENCE public.position_position_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.position_position_id_seq;
       public          postgres    false            �            1259    20191    user    TABLE     2  CREATE TABLE public."user" (
    user_id bigint NOT NULL,
    created_at timestamp without time zone,
    created_by character varying(255),
    updated_at timestamp without time zone,
    updated_by character varying(255),
    user_name character varying(255) NOT NULL,
    position_id bigint NOT NULL
);
    DROP TABLE public."user";
       public         heap    postgres    false            �            1259    20199    user_leave_request    TABLE     �  CREATE TABLE public.user_leave_request (
    user_leave_request_id bigint NOT NULL,
    created_at date,
    created_by character varying(255),
    tanggal_pengajuan date,
    description character varying(255) NOT NULL,
    leave_date_from date NOT NULL,
    leave_date_to date NOT NULL,
    sisa_cuti integer,
    submission_status character varying(255),
    updated_at date,
    updated_by character varying(255),
    user_id bigint NOT NULL
);
 &   DROP TABLE public.user_leave_request;
       public         heap    postgres    false            �            1259    20213 ,   user_leave_request_user_leave_request_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_leave_request_user_leave_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.user_leave_request_user_leave_request_id_seq;
       public          postgres    false            �            1259    20215    user_user_id_seq    SEQUENCE     y   CREATE SEQUENCE public.user_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.user_user_id_seq;
       public          postgres    false            )          0    20167    bucket_approval 
   TABLE DATA             COPY public.bucket_approval (bucket_approval_id, nama_pemohon, created_at, created_by, tanggal_pengajuan, resolved_by, resolved_date, pengambil_keputusan, alasan_keputusan, submission_status, updated_at, updated_by, user_id, user_leave_request_id) FROM stdin;
    public          postgres    false    202   */       *          0    20175    position 
   TABLE DATA           p   COPY public."position" (position_id, created_at, created_by, position_name, updated_at, updated_by) FROM stdin;
    public          postgres    false    203   G/       +          0    20183    position_leave 
   TABLE DATA           �   COPY public.position_leave (position_leave_id, created_at, created_by, jatah_cuti, updated_at, updated_by, position_id) FROM stdin;
    public          postgres    false    204   �/       ,          0    20191    user 
   TABLE DATA           q   COPY public."user" (user_id, created_at, created_by, updated_at, updated_by, user_name, position_id) FROM stdin;
    public          postgres    false    205   �/       -          0    20199    user_leave_request 
   TABLE DATA           �   COPY public.user_leave_request (user_leave_request_id, created_at, created_by, tanggal_pengajuan, description, leave_date_from, leave_date_to, sisa_cuti, submission_status, updated_at, updated_by, user_id) FROM stdin;
    public          postgres    false    206   %0       9           0    0 &   bucket_approval_bucket_approval_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.bucket_approval_bucket_approval_id_seq', 1, false);
          public          postgres    false    207            :           0    0 $   position_leave_position_leave_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.position_leave_position_leave_id_seq', 3, true);
          public          postgres    false    208            ;           0    0    position_position_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.position_position_id_seq', 12, true);
          public          postgres    false    209            <           0    0 ,   user_leave_request_user_leave_request_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.user_leave_request_user_leave_request_id_seq', 1, false);
          public          postgres    false    210            =           0    0    user_user_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.user_user_id_seq', 2, true);
          public          postgres    false    211            �
           2606    20174 $   bucket_approval bucket_approval_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.bucket_approval
    ADD CONSTRAINT bucket_approval_pkey PRIMARY KEY (bucket_approval_id);
 N   ALTER TABLE ONLY public.bucket_approval DROP CONSTRAINT bucket_approval_pkey;
       public            postgres    false    202            �
           2606    20190 "   position_leave position_leave_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.position_leave
    ADD CONSTRAINT position_leave_pkey PRIMARY KEY (position_leave_id);
 L   ALTER TABLE ONLY public.position_leave DROP CONSTRAINT position_leave_pkey;
       public            postgres    false    204            �
           2606    20182    position position_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public."position"
    ADD CONSTRAINT position_pkey PRIMARY KEY (position_id);
 B   ALTER TABLE ONLY public."position" DROP CONSTRAINT position_pkey;
       public            postgres    false    203            �
           2606    20206 *   user_leave_request user_leave_request_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.user_leave_request
    ADD CONSTRAINT user_leave_request_pkey PRIMARY KEY (user_leave_request_id);
 T   ALTER TABLE ONLY public.user_leave_request DROP CONSTRAINT user_leave_request_pkey;
       public            postgres    false    206            �
           2606    20198    user user_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pkey;
       public            postgres    false    205            �
           2606    20217 +   bucket_approval fk3o59dxt36t0x786k986hujtkq    FK CONSTRAINT     �   ALTER TABLE ONLY public.bucket_approval
    ADD CONSTRAINT fk3o59dxt36t0x786k986hujtkq FOREIGN KEY (user_id) REFERENCES public."user"(user_id);
 U   ALTER TABLE ONLY public.bucket_approval DROP CONSTRAINT fk3o59dxt36t0x786k986hujtkq;
       public          postgres    false    2723    205    202            �
           2606    20232     user fk4ej0twvfqwwu5xdcns6u2qne3    FK CONSTRAINT     �   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk4ej0twvfqwwu5xdcns6u2qne3 FOREIGN KEY (position_id) REFERENCES public."position"(position_id);
 L   ALTER TABLE ONLY public."user" DROP CONSTRAINT fk4ej0twvfqwwu5xdcns6u2qne3;
       public          postgres    false    2719    203    205            �
           2606    20237 -   user_leave_request fk6ic3d68w6432usb5385y1hvb    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_leave_request
    ADD CONSTRAINT fk6ic3d68w6432usb5385y1hvb FOREIGN KEY (user_id) REFERENCES public."user"(user_id);
 W   ALTER TABLE ONLY public.user_leave_request DROP CONSTRAINT fk6ic3d68w6432usb5385y1hvb;
       public          postgres    false    2723    205    206            �
           2606    20227 *   position_leave fk8mcbn5pj8d0qfdisqynvomvet    FK CONSTRAINT     �   ALTER TABLE ONLY public.position_leave
    ADD CONSTRAINT fk8mcbn5pj8d0qfdisqynvomvet FOREIGN KEY (position_id) REFERENCES public."position"(position_id);
 T   ALTER TABLE ONLY public.position_leave DROP CONSTRAINT fk8mcbn5pj8d0qfdisqynvomvet;
       public          postgres    false    2719    204    203            �
           2606    20222 +   bucket_approval fkobbjtmomfb9tmlkl24ai6rtj1    FK CONSTRAINT     �   ALTER TABLE ONLY public.bucket_approval
    ADD CONSTRAINT fkobbjtmomfb9tmlkl24ai6rtj1 FOREIGN KEY (user_leave_request_id) REFERENCES public.user_leave_request(user_leave_request_id);
 U   ALTER TABLE ONLY public.bucket_approval DROP CONSTRAINT fkobbjtmomfb9tmlkl24ai6rtj1;
       public          postgres    false    2725    202    206            )      x������ � �      *   4   x�3��!�܂����T��*\Z�ZT�Y�_�0�I�$��A�b���� �0R      +   @   x�3�4202�54�52V00�20 "Nǔ��<NC#�?2�2§��̈��2c�2c�=... ߻\      ,   :   x�3�4202�54�52V00�20 "Nǔ��<�?
M2��+�M,�4����� 0��      -      x������ � �     