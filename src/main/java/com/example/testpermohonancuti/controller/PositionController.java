package com.example.testpermohonancuti.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.testpermohonancuti.dtos.PositionDTO;
import com.example.testpermohonancuti.exceptions.IdNotFoundException;
import com.example.testpermohonancuti.models.Position;
import com.example.testpermohonancuti.repositories.PositionRepository;

@RestController
@RequestMapping("/api/position")
public class PositionController {

	@Autowired
	PositionRepository positionRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//Create
	@PostMapping("/create")
	public Map<String, Object> createPosition(@RequestBody PositionDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = modelMapper.map(body, Position.class);
		positionRepo.save(position);
		body.setPositionId(position.getPositionId());
		result.put("Message", "Create Position Success");
		result.put("Data", body);
		return result;
	}
	
	//Read All
	@GetMapping("/readall")
	public Map<String, Object> readAllPosition(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<PositionDTO> listPositionDTO = new ArrayList<>();
		List<Position> listPosition = positionRepo.findAll();
		for (Position position : listPosition) {
			PositionDTO positionDTO = modelMapper.map(position, PositionDTO.class);
			listPositionDTO.add(positionDTO);
		}
		result.put("Message", "Read All Position Success");
		result.put("Total", listPositionDTO.size());
		result.put("Data", listPositionDTO);
		return result;
	}
	
	//Read Single
	@GetMapping("/readsingle")
	public Map<String, Object> readSinglePosition(@RequestParam("positionid") Long positionId){
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = positionRepo.findById(positionId).orElseThrow(() -> new IdNotFoundException(String.format("Position ID %s tidak dapat ditemukan", positionId)));
		PositionDTO positionDTO = modelMapper.map(position, PositionDTO.class);
		result.put("Message", "Read Single Position Success");
		result.put("Data", positionDTO);
		return result;
	}
	
	//Update
	@PutMapping("/update")
	public Map<String, Object> updatePosition(@RequestParam("positionid") Long positionId, @RequestBody PositionDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = positionRepo.findById(positionId).orElseThrow(() -> new IdNotFoundException(String.format("Position ID %s tidak dapat ditemukan", positionId)));
		body.setPositionId(positionId);
		position = modelMapper.map(body, Position.class);
		positionRepo.save(position);
		result.put("Message", "Update Position Success");
		result.put("Data", body);
		return result;
	}
	
	//Delete
	@DeleteMapping("/delete")
	public Map<String, Object> deletePosition(@RequestParam("positionid") Long positionId){
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = positionRepo.findById(positionId).orElseThrow(() -> new IdNotFoundException(String.format("Position ID %s tidak dapat ditemukan", positionId)));
		PositionDTO positionDTO = modelMapper.map(position, PositionDTO.class);
		positionRepo.delete(position);
		result.put("Message", "Update Position Success");
		result.put("Data", positionDTO);
		return result;
	}
}
