package com.example.testpermohonancuti.dtos;

import java.util.Date;


public class BucketApprovalDTO {
	private long bucketApprovalId;
	private UserDTO user;
	private UserLeaveRequestDTO userLeaveRequest;
	private String namaPemohon;
	private Date createdAt;
	private String createdBy;
	private Date tanggalPengajuan;
	private String resolvedBy;
	private Date resolvedDate;
	private String pengambilKeputusan;
	private String alasanKeputusan;
	private String submissionStatus;
	private Date updatedAt;
	private String updatedBy;
	
	public BucketApprovalDTO() {
		// TODO Auto-generated constructor stub
	}

	public BucketApprovalDTO(long bucketApprovalId, UserDTO user, UserLeaveRequestDTO userLeaveRequest,
			String namaPemohon, Date createdAt, String createdBy, Date tanggalPengajuan, String resolvedBy,
			Date resolvedDate, String pengambilKeputusan, String alasanKeputusan, String submissionStatus,
			Date updatedAt, String updatedBy) {
		super();
		this.bucketApprovalId = bucketApprovalId;
		this.user = user;
		this.userLeaveRequest = userLeaveRequest;
		this.namaPemohon = namaPemohon;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.tanggalPengajuan = tanggalPengajuan;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.pengambilKeputusan = pengambilKeputusan;
		this.alasanKeputusan = alasanKeputusan;
		this.submissionStatus = submissionStatus;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
	}

	public long getBucketApprovalId() {
		return bucketApprovalId;
	}

	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public UserLeaveRequestDTO getUserLeaveRequest() {
		return userLeaveRequest;
	}

	public void setUserLeaveRequest(UserLeaveRequestDTO userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

	public String getNamaPemohon() {
		return namaPemohon;
	}

	public void setNamaPemohon(String namaPemohon) {
		this.namaPemohon = namaPemohon;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getTanggalPengajuan() {
		return tanggalPengajuan;
	}

	public void setTanggalPengajuan(Date tanggalPengajuan) {
		this.tanggalPengajuan = tanggalPengajuan;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public String getPengambilKeputusan() {
		return pengambilKeputusan;
	}

	public void setPengambilKeputusan(String pengambilKeputusan) {
		this.pengambilKeputusan = pengambilKeputusan;
	}

	public String getAlasanKeputusan() {
		return alasanKeputusan;
	}

	public void setAlasanKeputusan(String alasanKeputusan) {
		this.alasanKeputusan = alasanKeputusan;
	}

	public String getSubmissionStatus() {
		return submissionStatus;
	}

	public void setSubmissionStatus(String submissionStatus) {
		this.submissionStatus = submissionStatus;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
}
