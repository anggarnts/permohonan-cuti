package com.example.testpermohonancuti.dtos;

import java.util.Date;

public class PositionLeaveDTO {
	private long positionLeaveId;
	private PositionDTO position;
	private Date createdAt;
	private String createdBy;
	private int jatahCuti;
	private Date updatedAt;
	private String updatedBy;
	
	public PositionLeaveDTO() {
		// TODO Auto-generated constructor stub
	}

	public PositionLeaveDTO(long positionLeaveId, PositionDTO position, Date createdAt, String createdBy, int jatahCuti,
			Date updatedAt, String updatedBy) {
		super();
		this.positionLeaveId = positionLeaveId;
		this.position = position;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.jatahCuti = jatahCuti;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
	}

	public long getPositionLeaveId() {
		return positionLeaveId;
	}

	public void setPositionLeaveId(long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}

	public PositionDTO getPosition() {
		return position;
	}

	public void setPosition(PositionDTO position) {
		this.position = position;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public int getJatahCuti() {
		return jatahCuti;
	}

	public void setJatahCuti(int jatahCuti) {
		this.jatahCuti = jatahCuti;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
