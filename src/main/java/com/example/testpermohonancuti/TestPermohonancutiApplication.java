package com.example.testpermohonancuti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestPermohonancutiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestPermohonancutiApplication.class, args);
	}

}
