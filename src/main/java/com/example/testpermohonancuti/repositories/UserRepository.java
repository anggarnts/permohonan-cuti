package com.example.testpermohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.testpermohonancuti.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
